.PHONY: all clean

all: \
	permutations \
	combinations \
	string_match \
	knight_tour

clean: \
	clean_permutations \
	clean_combinations \
	clean_string_match \
	clean_knight_tour

permutations: permutations.c
	gcc --std=c99 -O2 -Wall -Wextra -pedantic permutations.c -o permutations
clean_permutations:
	rm -f permutations

combinations: combinations.c
	gcc --std=c99 -O2 -Wall -Wextra -pedantic combinations.c -o combinations
clean_combinations:
	rm -f combinations

string_match: string_match.c
	gcc --std=c99 -O2 -Wall -Wextra -pedantic string_match.c -o string_match
clean_string_match:
	rm -f string_match

knight_tour: knight_tour.c
	gcc --std=c99 -O2 -Wall -Wextra -pedantic knight_tour.c -o knight_tour
clean_knight_tour:
	rm -f knight_tour

