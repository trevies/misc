#include <stdio.h>
#include <stdlib.h>

void print(int *tab, int size)
{
    for (int i = 0; i < size; ++i)
        printf("%2d ", tab[i]);
    printf("\n");
}

void komb_impl(int *holder, int n, int k, int step)
{
    if (step == k + 1)
    {
        print(holder, k);
        return;
    }

    int start = 1;
    if (1 != step)
        start = holder[step - 2] + 1;

    for (int i = start; i <= n; ++i)
    {
        holder[step - 1] = i;
        if (k - step <= n - holder[step - 1]) // pruning step, crucial
            komb_impl(holder, n, k, step + 1);
    }
}

void komb(int n, int k)
{
    int *holder = (int *) malloc(k * sizeof(int));
    // do it as a recursive searching with backtracking
    komb_impl(holder, n, k, 1);
    free(holder);
}

int main()
{
    int n = 0, k = 0;
    printf("Give n: ");
    scanf("%d", &n);
    printf("Give k: ");
    scanf("%d", &k);
    printf("\nCombinations without repetitions, n = %d, k = %d:\n\n", n, k);
    komb(n, k);
    return 0;
}
