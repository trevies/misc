#include <stdio.h>
#include <stdlib.h>

void print(int *tab, int size)
{
    for (int i = 0; i < size; ++i)
        printf("%2d ", tab[i]);
    printf("\n");
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void perm_heap_impl(int *holder, int step, int size)
{
    if (step == 1)
    {
        print(holder, size);
        return;
    }

    for (int i = 0; i < step - 1; ++i)
    {
        perm_heap_impl(holder, step - 1, size);

        if (0 == step % 2)
            swap(&holder[i], &holder[step - 1]);
        else
            swap(&holder[0], &holder[step - 1]);
    }
    perm_heap_impl(holder, step - 1, size);
}

void perm_heap(int n)
{
    int *holder = (int *) malloc(n * sizeof(int));
    for (int i = 1; i <= n; ++i)
        holder[i - 1] = i;
    // permutate according to Heap algorithm
    perm_heap_impl(holder, n, n);
    free(holder);
}

int main()
{
    int n = 0;
    fprintf(stderr, "Give n: ");
    scanf("%d", &n);
    fprintf(stderr, "\nHeap's permutations of 1 .. %d:\n\n", n);
    perm_heap(n);
    return 0;
}

