#include <stdio.h>

int all_stars(const char *pattern)
{
    while (*pattern != '\0')
    {
        if (*pattern != '*')
            return 0;
        ++pattern;
    }

    return 1;
}

int str_match(const char *pattern, const char *str)
{
    // stop conditions
    if (str[0] == '\0') // no more string to be consumed
    {
        if (pattern[0] == '\0' || all_stars(pattern))
            return 1; // match
        else
            return 0; // no match
    }
    else if (pattern[0] == '\0') // there is a string to be consumed but no pattern
    {
        return 0; // no match
    }

    switch (pattern[0])
    {
        case '?':
            return str_match(pattern + 1, str + 1);

        case '*':
            return
                str_match(pattern, str + 1)
                | str_match(pattern + 1, str + 1)
                | str_match(pattern + 1, str);

        default:
            return pattern[0] == str[0] ? str_match(pattern + 1, str + 1) : 0;
    }
}

int main()
{
    char pattern[255] = { '\0' }, str[255] = { '\0' };

    printf("Pattern: ");
    scanf("%s", pattern);
    printf("String : ");
    scanf("%s", str);
    printf("\n");

    if (str_match(pattern, str))
    {
        printf("Match\n");
        return 0;
    }
    else
    {
        printf("No match\n");
        return 1;
    }
}
