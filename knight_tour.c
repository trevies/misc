#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    int rows;
    int cols;
} move;

#define NUM_MOVES 8
const move MOVES[NUM_MOVES] =
    {{-2,1},{-1,2},{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1}};

void print_hsep(int size)
{
    const char *sep = size * size > 99 ? "+---" : "+--";
    for (int i = 0; i < size; ++i)
         fputs(sep, stdout);
    printf("+\n");
}

void print(int *board, int size)
{
    print_hsep(size);
    const char *format = size * size > 99 ? "%3d|" : "%2d|";
    for (int row = 0; row < size; ++row)
    {
        printf("|");
        for (int col = 0; col < size; ++col)
            printf(format, board[col * size + row]);
        printf("\n");
        print_hsep(size);
    }
}

int is_number_in_board(int *board, int size, int val)
{
    for (int row = 0; row < size; ++row)
        for (int col = 0; col < size; ++col)
            if (board[row * size + col] == val)
                return 1;
    return 0;
}

int verify(int *board, int size)
{
    for (int i = 1; i <= size * size; ++i)
        if (!is_number_in_board(board, size, i))
                return 0;
    return 1;
}

int is_within_board(int row, int col, int size)
{
    if (row < 0 || row >= size || col < 0 || col >= size)
        return 0;
    return 1;
}

int is_valid_move(int *board, int row, int col, int size)
{
    if (!is_within_board(row, col, size))
        return 0;
    if (0 != board[row * size + col])
        return 0;
    return 1;
}

int get_number_of_successors(int *board, int size, int row, int col)
{
    int number_of_successors = 0;

    for (int i = 0; i < NUM_MOVES; ++i)
    {
        int new_row = MOVES[i].rows + row;
        int new_col = MOVES[i].cols + col;

        if (!is_valid_move(board, new_row, new_col, size))
            continue;

        ++number_of_successors;
    }

    return number_of_successors;
}

move get_successor(int *board, int size, int row, int col)
{
    move move_cand = {0, 0};
    int cand_rating = NUM_MOVES + 1;

    for (int i = 0; i < NUM_MOVES; ++i)
    {
        int new_row = MOVES[i].rows + row;
        int new_col = MOVES[i].cols + col;

        if (!is_valid_move(board, new_row, new_col, size))
            continue;

        int number_of_successors =
                        get_number_of_successors(board, size, new_row, new_col);
        if (number_of_successors < cand_rating)
        {
            cand_rating = number_of_successors;
            move_cand = MOVES[i];
        }

        if (cand_rating == 1)
            break;
    }

    return move_cand;
}

int knights_tour_impl(int *board, int size, int row, int col)
{
    int move_count = board[row * size + col];
    if (move_count == size * size)
        return 1;

    move move_cand = get_successor(board, size, row, col);
    if (move_cand.rows == 0)
    {
        fprintf(stderr, "ERROR!?!\n");
        return 0;
    }

    int new_row = move_cand.rows + row;
    int new_col = move_cand.cols + col;
    board[new_row * size + new_col] = move_count + 1;

    return knights_tour_impl(board, size, new_row, new_col);
}

int knights_tour(int size, int row, int col)
{
    int num_fields = size * size * sizeof(int);
    int *board = (int *) malloc(num_fields);

    memset(board, 0, num_fields);
    board[row * size + col] = 1;

    // solve the knight's tour with Warnsdorff algorithm
    int ret = knights_tour_impl(board, size, row, col);

    if (1 == ret)
    {
        print(board, size);
        if (!verify(board, size))
            fprintf(stderr, "Above solution is invalid!\n");
    }
    else
    {
        fprintf(stderr, "No solution found!\n");
    }

    free(board);
    return ret;
}

int main()
{
    int size = 0, row = 0, col = 0;

    printf("Give the size of chessboard: ");
    scanf("%d", &size);
    printf("Give starting row: ");
    scanf("%d", &row);
    printf("Give starting col: ");
    scanf("%d", &col);

    if (size < 3)
    {
        fprintf(stderr, "Wrong size!\n");
        return 1;
    }
    if (row >= size || col >= size)
    {
        fprintf(stderr, "Wrong indices!\n");
        return 1;
    }

    return !knights_tour(size, row, col);
}
